package com.example.azra.projekat;

import android.os.AsyncTask;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Azra on 10.5.2018.
 */

public class DohvatiNajnovije extends AsyncTask<String, Integer, Void> {

    private String[] imeAutora;
    private ArrayList<Knjiga> listaKnjiga = new ArrayList<>();

    private IDohvatiNajnovijeDone pozivatelj;

    public DohvatiNajnovije(IDohvatiNajnovijeDone p) {
        pozivatelj = p;
    }

    @Override
    protected Void doInBackground(String... params) {
        String query = null;
        try {
            query = URLEncoder.encode(params[0], "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        imeAutora = params;

        String url1 = "https://www.googleapis.com/books/v1/volumes?q=inauthor:" + imeAutora[0] + "&maxResults=5" + "&orderBy=newest";
        try {
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);
            JSONArray items = jo.getJSONArray("items");
            for (int i = 0; i < items.length(); i++) {
                JSONObject artist = items.getJSONObject(i);
                JSONObject volumeInfo = artist.getJSONObject("volumeInfo");
                String name = volumeInfo.getString("title");
                String id = artist.getString("id");
                int brojStranica = 0;
                if (volumeInfo.has("pageCount")) brojStranica = volumeInfo.getInt("pageCount");
                String opis = "";
                if (volumeInfo.has("description")) opis = volumeInfo.getString("description");
                JSONArray autori = new JSONArray();
                if (volumeInfo.has("authors")) autori = volumeInfo.getJSONArray("authors");
                ArrayList<Autor> imenaAutora = new ArrayList<>();
                String imeAutora = "";
                for (int j = 0; j < autori.length(); j++) {

                    Autor a = new Autor(autori.getString(j), id);
                    a.kolikoDjela++;
                    imenaAutora.add(a);
                    if (j == autori.length() - 1) imeAutora += imenaAutora.get(j).getIme();
                    else imeAutora += imenaAutora.get(j).getIme() + ", ";
                }

                String datumObjavljivanja = "";
                if (volumeInfo.has("publishedDate"))
                    datumObjavljivanja = volumeInfo.getString("publishedDate");
                JSONObject slicica = new JSONObject();
                if (volumeInfo.has("imageLinks")) slicica = volumeInfo.getJSONObject("imageLinks");
                String image = "https://scontent.fsjj2-1.fna.fbcdn.net/v/t1.15752-9/32506473_10204232985902317_901854982251216896_n.png?_nc_cat=0&oh=99f7ef8d31e4e0d8b8181604da391c2d&oe=5B93F38F";
                if (slicica.has("thumbnail")) {
                    image = slicica.getString("thumbnail");
                }
                URL slika = new URL(image);
                Knjiga rez = new Knjiga(id, name, imenaAutora, opis, datumObjavljivanja, slika, brojStranica);
                Autor autor = new Autor(imeAutora, id);
                rez.setAutora(autor);
                listaKnjiga.add(rez);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }


    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        pozivatelj.onNajnovijeDone(listaKnjiga);
    }


    public interface IDohvatiNajnovijeDone {
        public void onNajnovijeDone(ArrayList<Knjiga> rez);
    }

}

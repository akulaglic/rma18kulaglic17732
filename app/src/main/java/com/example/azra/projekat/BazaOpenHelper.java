package com.example.azra.projekat;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.CursorAdapter;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Created by Azra on 23.5.2018.
 */

public class BazaOpenHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "Baza.db";
    public static final String DATABASE_TABLE_KATEGORIJA = "Kategorija";
    public static final String DATABASE_TABLE_KNJIGA = "Knjiga";
    public static final String DATABASE_TABLE_AUTOR = "Autor";
    public static final String DATABASE_TABLE_AUTORSTVO = "Autorstvo";
    public static final int DATABASE_VERSION = 1;
    public static final String KATEGORIJE_ID = "_id";
    public static final String KATEGORIJE_NAZIV = "naziv";
    public static final String KNJIGA_ID = "_id";
    public static final String KNJIGA_NAZIV = "naziv";
    public static final String KNJIGA_OPIS = "opis";
    public static final String KNJIGA_DATUM_OBJAVLJIVANJA = "datumObjavljivanja";
    public static final String KNJIGA_BROJ_STRANICA = "brojStranica";
    public static final String KNJIGA_ID_WEB_SERVIS = "idWebServis";
    public static final String KNJIGA_SLIKA= "slika";
    public static final String KNJIGA_PREGLEDANA = "pregledana";
    public static final String KNJIGA_ID_KATEGORIJE = "idkategorije";
    public static final String AUTOR_ID = "_id";
    public static final String AUTOR_IME = "ime";
    public static final String AUTORSTVO_ID = "_id";
    public static final String AUTORSTVO_ID_AUTORA = "idautora";
    public static final String AUTORSTVO_ID_KNJIGE = "idknjige";
    private SQLiteDatabase myDataBase;
    public final static String DATABASE_PATH = "/data/data/com.pkgname/databases/";


    private static final String DATABASE_CREATE_KATEGORIJE = "create table " + DATABASE_TABLE_KATEGORIJA
            + " (" + KATEGORIJE_ID + " integer primary key autoincrement, " +
            KATEGORIJE_NAZIV + " text not null);";

    private static final String DATABASE_CREATE_KNJIGA = "create table " + DATABASE_TABLE_KNJIGA
            + " (" + KNJIGA_ID + " integer primary key autoincrement, " +
            KNJIGA_NAZIV + " text not null, " + KNJIGA_OPIS + " text not null, " +
            KNJIGA_DATUM_OBJAVLJIVANJA + " text not null, " + KNJIGA_BROJ_STRANICA + " integer, "
            + KNJIGA_ID_WEB_SERVIS + " text not null, " + KNJIGA_SLIKA +" text not null, "
            + KNJIGA_PREGLEDANA +" integer, "+ KNJIGA_ID_KATEGORIJE + " integer);";

    private static final String DATABASE_CREATE_AUTOR = "create table " + DATABASE_TABLE_AUTOR
            + " (" + AUTOR_ID + " integer primary key autoincrement, " + AUTOR_IME + " text not null);";


    private static final String DATABASE_CREATE_AUTORSTVO = "create table " + DATABASE_TABLE_AUTORSTVO
            + " (" + AUTORSTVO_ID + " integer primary key autoincrement, " + AUTORSTVO_ID_AUTORA + " integer, "
            + AUTORSTVO_ID_KNJIGE + " integer);";


    private static BazaOpenHelper objLogger;

    public static BazaOpenHelper getInstance()
    {
        if (objLogger == null)
        {
            objLogger = new BazaOpenHelper();
        }
        return objLogger;
    }


    public  BazaOpenHelper (Context c) {
        super(c, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public BazaOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public BazaOpenHelper() {
        super(null, null, null, DATABASE_VERSION);
    }

    public void openDatabase() throws SQLException {
        String PATH = DATABASE_NAME;
        myDataBase = SQLiteDatabase.openDatabase(PATH, null, SQLiteDatabase.OPEN_READWRITE);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DATABASE_CREATE_KATEGORIJE);
        sqLiteDatabase.execSQL(DATABASE_CREATE_KNJIGA);
        sqLiteDatabase.execSQL(DATABASE_CREATE_AUTOR);
        sqLiteDatabase.execSQL(DATABASE_CREATE_AUTORSTVO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_KATEGORIJA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_KNJIGA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_AUTOR);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_AUTORSTVO);

        onCreate(sqLiteDatabase);
    }
    public ArrayList<String> procitajKategoriju() {

        ArrayList lista = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM "+DATABASE_TABLE_KATEGORIJA;
        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    lista.add(cursor.getString(cursor.getColumnIndex(KATEGORIJE_NAZIV)));
                } while (cursor.moveToNext());

            }
            cursor.close();
        }

        cursor.close();

        return lista;
    }

    public long dodajKategoriju(String naziv) {
        long id = -1;

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues novi = new ContentValues();
        novi.put(KATEGORIJE_NAZIV, naziv);

        String query = "Select * from " + DATABASE_TABLE_KATEGORIJA +" where "+KATEGORIJE_NAZIV+"='"+ naziv+"'";
        Cursor cursor = db.rawQuery(query, null);

        if(cursor.getCount() == 0 ){
            id = db.insert(BazaOpenHelper.DATABASE_TABLE_KATEGORIJA, null, novi);
        }

        cursor.close();

       return id;
    }

    public long dodajKnjigu(Knjiga knjiga) {
        long id = -1;

        SQLiteDatabase db;

        ContentValues novi = new ContentValues();
        novi.put(KNJIGA_NAZIV, knjiga.getNazivKnjige());
        novi.put(KNJIGA_BROJ_STRANICA, knjiga.getBrojStranica());
        novi.put(KNJIGA_DATUM_OBJAVLJIVANJA, knjiga.getDatumObjavljivanja());
        novi.put(KNJIGA_OPIS, knjiga.getOpis());
        novi.put(KNJIGA_SLIKA, knjiga.getSlika().toString());
        novi.put(KNJIGA_ID_WEB_SERVIS, knjiga.getId().toString());
        novi.put(KNJIGA_PREGLEDANA, 0);

        db = this.getReadableDatabase();
        String upit = "Select * from "+DATABASE_TABLE_KATEGORIJA+ " where " + KATEGORIJE_NAZIV +"='" + knjiga.getKategorija()+"'";
        Cursor c = db.rawQuery(upit, null);

        c.moveToNext();
        long d = c.getLong(c.getColumnIndex(KATEGORIJE_ID));

        novi.put(KNJIGA_ID_KATEGORIJE, d);
        c.close();

        db = this.getWritableDatabase();
        ContentValues noviAutorstvo = new ContentValues();
        ContentValues noviAutor= new ContentValues();

        db = this.getReadableDatabase();

        String query = "Select * from " + DATABASE_TABLE_KNJIGA +" where "+ KNJIGA_ID_WEB_SERVIS +"='"+knjiga.getId()+"'";
        Cursor cursor = db.rawQuery(query, null);


        db = this.getWritableDatabase();
        if(cursor.getCount() == 0 ){
            id = db.insert(BazaOpenHelper.DATABASE_TABLE_KNJIGA, null, novi);

            for (int i=0; i<knjiga.getAutori().size(); i++) {
                noviAutor.put(AUTOR_IME, knjiga.getAutori().get(i).getIme());
                String queryy = "Select * from " + DATABASE_TABLE_AUTOR +" where "+AUTOR_IME+"='"+ knjiga.getAutori().get(i).getIme()+"'";
                Cursor cursor1 = db.rawQuery(queryy, null);

                if(cursor1.getCount() == 0 ) {
                    long idAutora = db.insert(BazaOpenHelper.DATABASE_TABLE_AUTOR, null, noviAutor);
                    noviAutorstvo.put(AUTORSTVO_ID_AUTORA, idAutora);
                }
                else {
                    cursor1.moveToNext();
                    long d1 = cursor1.getLong(cursor1.getColumnIndex(AUTOR_ID));
                    noviAutorstvo.put(AUTORSTVO_ID_AUTORA, d1 );
                }

                noviAutorstvo.put(AUTORSTVO_ID_KNJIGE, id);
                db.insert(BazaOpenHelper.DATABASE_TABLE_AUTORSTVO, null, noviAutorstvo);
                cursor1.close();

            }
        }
        cursor.close();

        return  id;
    }

    public ArrayList<Knjiga> knjigeKategorije(long idKategorije) {
        ArrayList<Knjiga> lista = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        String upit = "Select * from "+DATABASE_TABLE_KNJIGA + " where "+ KNJIGA_ID_KATEGORIJE + "=" + idKategorije + "";
        Cursor c = db.rawQuery(upit, null);

        String upit1 = "Select * from "+DATABASE_TABLE_KATEGORIJA + " where "+ KATEGORIJE_ID + "=" + idKategorije + "";
        Cursor cur = db.rawQuery(upit1, null);

        cur.moveToNext();
        for(int i=0; i<c.getCount(); i++) {
            c.moveToNext();
            Knjiga k = new Knjiga();
            String s = c.getString(c.getColumnIndex(KNJIGA_SLIKA));
            try {
                URL u = new URL(s);
                k.setSlika(u);
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            }

            k.setOpis(c.getString(c.getColumnIndex(KNJIGA_OPIS)));
            k.setBrojStranica(c.getInt(c.getColumnIndex(KNJIGA_BROJ_STRANICA)));
            k.setDatumObjavljivanja(c.getString(c.getColumnIndex(KNJIGA_DATUM_OBJAVLJIVANJA)));
            k.setNazivKnjige(c.getString(c.getColumnIndex(KNJIGA_NAZIV)));
            long id = c.getLong(c.getColumnIndex(KNJIGA_ID));
            k.setId(String.valueOf(id));
            k.setKategorija(cur.getString(cur.getColumnIndex(KATEGORIJE_NAZIV)));

            String upit2 = "select * from " + DATABASE_TABLE_AUTOR +" a, "+ DATABASE_TABLE_AUTORSTVO +
                    " a1 where a1.idautora=a._id and  a1." + AUTORSTVO_ID_KNJIGE + "=" + id;
            Cursor cursor1 = db.rawQuery(upit2, null);
            cursor1.moveToNext();

            ArrayList<Autor> autori = new ArrayList<>();
            Autor a = new Autor();
            String ime1 = cursor1.getString(cursor1.getColumnIndex(AUTOR_IME));
            a.setIme(ime1);
            ArrayList<String> d = autorSaViseDijela(cursor1.getLong(cursor1.getColumnIndex(AUTOR_ID)));
            a.setDjelo(d);
            autori.add(a);
            k.setAutori(autori);

            String d1 = autorSaViseDijelaImeAutora(c.getLong(c.getColumnIndex(KNJIGA_ID)));
            k.setImenaAutora(d1);

            lista.add(k);
            cursor1.close();

        }
        cur.close();

        c.close();

        return lista;
    }

    public ArrayList<Knjiga> knjigeAutora(long idAutora) {
        ArrayList<Knjiga> lista = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        String upit = "select * from " + DATABASE_TABLE_AUTOR +" a, " +DATABASE_TABLE_AUTORSTVO+ " a1, " +
                DATABASE_TABLE_KNJIGA+" k where a1.idautora=a._id and k._id=a1.idknjige and a."
                + AUTOR_ID + "=" + idAutora;
        Cursor c = db.rawQuery(upit, null);

        for(int i=0; i<c.getCount(); i++) {
            c.moveToNext();
            Knjiga k = new Knjiga();
            k.setNazivKnjige(c.getString(c.getColumnIndex(KNJIGA_NAZIV)));
            String s = c.getString(c.getColumnIndex(KNJIGA_SLIKA));
            try {
                URL u = new URL(s);
                k.setSlika(u);
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            }
            k.setOpis(c.getString(c.getColumnIndex(KNJIGA_OPIS)));
            k.setBrojStranica(c.getInt(c.getColumnIndex(KNJIGA_BROJ_STRANICA)));
            k.setDatumObjavljivanja(c.getString(c.getColumnIndex(KNJIGA_DATUM_OBJAVLJIVANJA)));
            long id = c.getLong(c.getColumnIndex(KNJIGA_ID));
            k.setId(String.valueOf(id));

            String opis1 = "select * from "+DATABASE_TABLE_KNJIGA+" k, "+ DATABASE_TABLE_KATEGORIJA +
                    " k1 where k.idkategorije=k1._id and k." + KNJIGA_ID +"=" + c.getLong(c.getColumnIndex(KNJIGA_ID));
            Cursor cur = db.rawQuery(opis1, null);
            cur.moveToNext();
            k.setKategorija(cur.getString(cur.getColumnIndex(KATEGORIJE_NAZIV)));
            cur.close();

            ArrayList<Autor> autori = new ArrayList<>();
            Autor a = new Autor();
            String ime1 = c.getString(c.getColumnIndex(AUTOR_IME));
            a.setIme(ime1);
            ArrayList<String> d = autorSaViseDijela(c.getLong(c.getColumnIndex(AUTOR_ID)));
            a.setDjelo(d);
            autori.add(a);
            k.setAutori(autori);

            String d1 = autorSaViseDijelaImeAutora(c.getLong(c.getColumnIndex(KNJIGA_ID)));
            k.setImenaAutora(d1);
            lista.add(k);
        }

        c.close();

        return lista;
    }



    public long dajIdKategorije (String kategorija) {
        long id;

        SQLiteDatabase db = this.getReadableDatabase();
        String s = "select * from " + DATABASE_TABLE_KATEGORIJA + " where " + KATEGORIJE_NAZIV + "='" + kategorija +"'";
        Cursor c = db.rawQuery(s, null);
        c.moveToNext();

        id = c.getLong(c.getColumnIndex(KATEGORIJE_ID));

        c.close();
        return id;
    }

    public long dajIdAutora (String autor) {
        long id;

        SQLiteDatabase db = this.getReadableDatabase();
        String s = "select * from " + DATABASE_TABLE_AUTOR + " where " + AUTOR_IME+ "='" + autor +"'";
        Cursor c = db.rawQuery(s, null);
        c.moveToNext();

        id = c.getLong(c.getColumnIndex(AUTOR_ID));

        c.close();
        return id;
    }

    public long dajIdKnjige (String knjiga, String datum) {
        long id;

        SQLiteDatabase db = this.getReadableDatabase();
        String s = "select * from " + DATABASE_TABLE_KNJIGA + " where " + KNJIGA_NAZIV + "='" + knjiga +"' and "+ KNJIGA_DATUM_OBJAVLJIVANJA
                + "='"+datum + "'";
        Cursor c = db.rawQuery(s, null);
        c.moveToNext();

        id = c.getLong(c.getColumnIndex(KNJIGA_ID));

        c.close();
        return id;
    }



    public boolean daLiJePraznaBaza(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCursor = db.rawQuery("SELECT * FROM " + DATABASE_TABLE_KATEGORIJA, null);
        Boolean rowExists;
        if (mCursor.moveToFirst()) rowExists = true;
        else rowExists = false;
        return rowExists;
    }
    public boolean daLiJePraznaBaza2(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCursor = db.rawQuery("SELECT * FROM " + DATABASE_TABLE_AUTOR, null);
        Boolean rowExists;
        if (mCursor.moveToFirst()) rowExists = true;
        else rowExists = false;
        return rowExists;
    }

    public boolean daLiJePraznaBaza1(long id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCursor = db.rawQuery("SELECT * FROM " + DATABASE_TABLE_KNJIGA+ " where "
                + KNJIGA_ID_KATEGORIJE + "="+ id, null);
        Boolean rowExists;
        if (mCursor.moveToFirst()) rowExists = true;
        else rowExists = false;
        return rowExists;
    }

    public ArrayList<Autor> autoriIzBaze () {
        ArrayList<Autor> autori = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        String upit = "select distinct ime, _id from " + DATABASE_TABLE_AUTOR;
        Cursor c = db.rawQuery(upit, null);

        for(int i=0; i<c.getCount(); i++) {
            c.moveToNext();
            Autor a = new Autor();
            a.setIme(c.getString(c.getColumnIndex(AUTOR_IME)));
            ArrayList<String> djela = new ArrayList<>();
            long gg = c.getLong(c.getColumnIndex(AUTOR_ID));
            djela = autorSaViseDijela(gg);

            a.setKolikoDjela(djela.size());
           autori.add(a);

        }
        c.close();

        return autori;
    }

    public  ArrayList<String> autorSaViseDijela (long id) {
        ArrayList<String> l = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String upit = "select * from " +DATABASE_TABLE_AUTORSTVO +" a, "+DATABASE_TABLE_KNJIGA
                + " k where a.idknjige=k._id and " + AUTORSTVO_ID_AUTORA + "=" + id;
        Cursor c = db.rawQuery(upit, null);

        for(int i=0; i<c.getCount(); i++) {
            c.moveToNext();
            l.add(c.getString(c.getColumnIndex(KNJIGA_NAZIV)));
        }

        c.close();
        return l;
    }


    public  String autorSaViseDijelaImeAutora (long id) {
        String l ="";
        SQLiteDatabase db = this.getReadableDatabase();
        String upit = "select * from " +DATABASE_TABLE_AUTORSTVO +" a, "+DATABASE_TABLE_KNJIGA
                + " k, "+DATABASE_TABLE_AUTOR+ " a1" + " where a.idknjige=k._id and a.idautora=a1._id and "
                + AUTORSTVO_ID_KNJIGE+ "=" + id;
        Cursor c = db.rawQuery(upit, null);

        for(int i=0; i<c.getCount()-1; i++) {
            c.moveToNext();
            l+= c.getString(c.getColumnIndex(AUTOR_IME)) +", ";
        }
        c.moveToNext();
        l += c.getString(c.getColumnIndex(AUTOR_IME));

        c.close();
        return l;
    }

    public void promijeniPregledanoZaKnjigu (long id) {
        SQLiteDatabase db;
        db = this.getReadableDatabase();
        String s = "select * from " + DATABASE_TABLE_KNJIGA +" where "+KNJIGA_ID +"=" +id;

        Cursor c = db.rawQuery(s, null);

        c.moveToNext();
        ContentValues novi = new ContentValues();
        novi.put(KNJIGA_PREGLEDANA, 1);
       // novi.put(KNJIGA_ID, id);
        novi.put(KNJIGA_ID_WEB_SERVIS, c.getString(c.getColumnIndex(KNJIGA_ID_WEB_SERVIS)));
        novi.put(KNJIGA_SLIKA, c.getString(c.getColumnIndex(KNJIGA_SLIKA)));
        novi.put(KNJIGA_ID_KATEGORIJE, c.getLong(c.getColumnIndex(KNJIGA_ID_KATEGORIJE)));
        novi.put(KNJIGA_OPIS, c.getString(c.getColumnIndex(KNJIGA_OPIS)));
        novi.put(KNJIGA_BROJ_STRANICA, c.getInt(c.getColumnIndex(KNJIGA_BROJ_STRANICA)));
        novi.put(KNJIGA_DATUM_OBJAVLJIVANJA, c.getString(c.getColumnIndex(KNJIGA_DATUM_OBJAVLJIVANJA)));
        novi.put(KNJIGA_NAZIV, c.getString(c.getColumnIndex(KNJIGA_NAZIV)));


        db.update(DATABASE_TABLE_KNJIGA, novi, "_id="+id, null);

        c.close();
    }

}

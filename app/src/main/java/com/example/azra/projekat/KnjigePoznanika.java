package com.example.azra.projekat;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Azra on 10.5.2018.
 */

public class KnjigePoznanika extends IntentService {

    private ArrayList<Knjiga> listaKnjiga = new ArrayList<>();

    public static final int STATUS_START = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;

    public KnjigePoznanika(String name) {
        super(name);
    }

    public KnjigePoznanika() {
        super(null);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        String korisnik = intent.getStringExtra("korisnik");

        Bundle bundle = new Bundle();

        receiver.send(STATUS_START, Bundle.EMPTY);
        String url1 = "https://www.googleapis.com/books/v1/users/" + korisnik + "/bookshelves";

        ArrayList<Integer> idBookshelvea = new ArrayList<>();
        try {
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);
            JSONArray items = jo.getJSONArray("items");

            for (int i = 0; i < items.length(); i++) {
                JSONObject item = new JSONObject();
                item = (JSONObject) items.get(i);
                int id = 0;
                if (item.has("id")) id = item.getInt("id");
                idBookshelvea.add(id);
            }
        } catch (MalformedURLException e) {
            receiver.send(STATUS_ERROR, Bundle.EMPTY);
            e.printStackTrace();
        } catch (IOException e) {
            receiver.send(STATUS_ERROR, Bundle.EMPTY);
            e.printStackTrace();
        } catch (JSONException e) {
            receiver.send(STATUS_ERROR, Bundle.EMPTY);
            e.printStackTrace();
        }
        String url2 = null;
        for (int k = 0; k < idBookshelvea.size(); k++) {
            url2 = "https://www.googleapis.com/books/v1/users/" + korisnik + "/bookshelves/" + idBookshelvea.get(k).toString()
                    + "/volumes";

            try {
                URL url = new URL(url2);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String rezultat = convertStreamToString(in);
                JSONObject jo = new JSONObject(rezultat);
                JSONArray items = jo.getJSONArray("items");

                for (int i = 0; i < items.length(); i++) {
                    JSONObject artist = items.getJSONObject(i);
                    JSONObject volumeInfo = artist.getJSONObject("volumeInfo");
                    String name = volumeInfo.getString("title");
                    String id = artist.getString("id");
                    int brojStranica = 0;
                    if (volumeInfo.has("pageCount")) brojStranica = volumeInfo.getInt("pageCount");
                    String opis = "";
                    if (volumeInfo.has("description")) opis = volumeInfo.getString("description");
                    JSONArray autori = new JSONArray();
                    if (volumeInfo.has("authors")) autori = volumeInfo.getJSONArray("authors");
                    ArrayList<Autor> imenaAutora = new ArrayList<>();
                    String imeAutora = "";
                    for (int j = 0; j < autori.length(); j++) {

                        Autor a = new Autor(autori.getString(j), id);
                        a.kolikoDjela++;
                        imenaAutora.add(a);
                        if (j == autori.length() - 1) imeAutora += imenaAutora.get(j).getIme();
                        else imeAutora += imenaAutora.get(j).getIme() + ", ";
                    }
                    String datumObjavljivanja = "";
                    if (volumeInfo.has("publishedDate"))
                        datumObjavljivanja = volumeInfo.getString("publishedDate");
                    JSONObject slicica = new JSONObject();
                    if (volumeInfo.has("imageLinks"))
                        slicica = volumeInfo.getJSONObject("imageLinks");
                    String image = "https://scontent.fsjj2-1.fna.fbcdn.net/v/t1.15752-9/32506473_10204232985902317_901854982251216896_n.png?_nc_cat=0&oh=99f7ef8d31e4e0d8b8181604da391c2d&oe=5B93F38F";
                    if (slicica.has("thumbnail")) {
                        image = slicica.getString("thumbnail");
                    }
                    URL slika = new URL(image);
                    Knjiga rez = new Knjiga(id, name, imenaAutora, opis, datumObjavljivanja, slika, brojStranica);
                    Autor autor = new Autor(imeAutora, id);
                    rez.setAutora(autor);
                    listaKnjiga.add(rez);
                }

            } catch (MalformedURLException e) {
                receiver.send(STATUS_ERROR, Bundle.EMPTY);
                e.printStackTrace();
            } catch (IOException e) {
                receiver.send(STATUS_ERROR, Bundle.EMPTY);
                e.printStackTrace();
            } catch (JSONException e) {
                receiver.send(STATUS_ERROR, Bundle.EMPTY);
                e.printStackTrace();
            }
        }

        bundle.putParcelableArrayList("result", listaKnjiga);
        receiver.send(STATUS_FINISHED, bundle);
    }


    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }

}

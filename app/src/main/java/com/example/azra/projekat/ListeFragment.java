package com.example.azra.projekat;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ResourceCursorAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.facebook.stetho.Stetho;

import java.util.ArrayList;

/**
 * Created by Azra on 1.4.2018.
 */

public class ListeFragment extends Fragment {

    private ArrayList<String> unosi = new ArrayList<>();
    private ArrayList<Knjiga> knjiga = new ArrayList<>();
    private ArrayAdapter<String> adapter;
    private byte[] byteNiz;
    private ArrayList<Integer> itemToSetBg = null;
    private ArrayList<String> kategorije = new ArrayList<>();
    private ListView lista;
    private ArrayList<String> listaAutora = new ArrayList<>();
    private ArrayList<Autor> autori = new ArrayList<>();
    private String fragmentOtvoren ;
    private ArrayList<String> autoriImena = new ArrayList<>();
    private long id;
    BazaOpenHelper db;

    public static ListeFragment newInstance(Bundle bundle) {

        Bundle args = new Bundle();
        args = bundle;
        ListeFragment fragment = new ListeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.liste_fragment, container, false);
        Stetho.initializeWithDefaults(getActivity());

        db = new BazaOpenHelper(getActivity());

        if (getArguments() != null) {
            knjiga = (ArrayList<Knjiga>)getArguments() .getSerializable("knjiga");
            kategorije = getArguments() .getStringArrayList("listakategorija");
            byteNiz = getArguments() .getByteArray("slika");
            itemToSetBg = getArguments() .getIntegerArrayList("listItemToSetBackground");
            autori = (ArrayList<Autor>) getArguments().getSerializable("autori");
            fragmentOtvoren = getArguments().getString("fragment");
            autoriImena = getArguments().getStringArrayList("autoriNova");
            id = getArguments().getLong("id");

        }
        fragmentOtvoren = "kategorije";

        final EditText txt = (EditText) view.findViewById(R.id.tekstPretraga);
        final Button pretraga = (Button) view.findViewById(R.id.dPretraga);
        final Button dugmeKategorija = (Button) view.findViewById(R.id.dDodajKategoriju);
        lista = (ListView) view.findViewById(R.id.listaKategorija);
        final Button dugmeKnjiga = (Button) view.findViewById(R.id.dDodajKnjigu);
        if (kategorije != null && unosi != null) unosi.addAll(0, kategorije);
        if (autoriImena != null) listaAutora.addAll(0, autoriImena);
        Button dugmeListaKategorija = (Button) view.findViewById(R.id.dKategorije);
        Button dugmeListaAutora = (Button) view.findViewById(R.id.dAutori);
        Button dodajOnline = (Button) view.findViewById(R.id.dDodajOnline);


        if(db.daLiJePraznaBaza()) {
           if(!txt.getText().toString().equals("")) db.dodajKategoriju(txt.getText().toString());
            unosi = db.procitajKategoriju();
            adapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.element_liste, R.id.tekst, unosi);

            lista.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }

        /*else if (!unosi.isEmpty() || !listaAutora.isEmpty()) {
            db.dodajKategoriju(txt.getText().toString());
            unosi = db.procitajKategoriju();
            adapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.element_liste, R.id.tekst, unosi);

            lista.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }*/
        dugmeKategorija.setEnabled(false);

        dugmeKategorija.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                long n = db.dodajKategoriju(txt.getText().toString());
                unosi = db.procitajKategoriju();
                //unosi.add(txt.getText().toString());

                adapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.element_liste, R.id.tekst, unosi);
                lista.setAdapter(adapter);
                adapter.notifyDataSetChanged();

                txt.setText("");

                dugmeKategorija.setEnabled(false);
            }
        });

        pretraga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String string = txt.getText().toString();
                if(adapter != null) {
                    adapter.getFilter().filter(string);

                    adapter.notifyDataSetChanged();
                }
                    int br = 0;
                    for (String s : unosi) if (s.toLowerCase().contains(string.toLowerCase())) br++;

                    if (br == 0) {
                        dugmeKategorija.setEnabled(true);
                    } else dugmeKategorija.setEnabled(false);


            }
        });

        dugmeKnjiga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("kategorije", unosi);
                bundle.putSerializable("knjige", knjiga);
                bundle.putSerializable("autori", autori);

                ListeFragment frag = new ListeFragment();
                frag.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();

                fragmentManager.beginTransaction().replace(R.id.mjestoF1, DodavanjeKnjigeFragment.newInstance(bundle)).commit();
            }
        });

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ArrayList<Knjiga> novaListaKnjiga = new ArrayList<>();
                Bundle bundle = new Bundle();
                if(fragmentOtvoren.equals("kategorije")) {

                    id = db.dajIdKategorije(adapterView.getItemAtPosition(i).toString());

                    if (knjiga != null) {
                        for (int j = 0; j < knjiga.size(); j++) {
                            if (knjiga.get(j).getKategorija().equals(adapterView.getItemAtPosition(i).toString())) {
                                novaListaKnjiga.add(knjiga.get(j));
                                id = db.dajIdKategorije(adapterView.getItemAtPosition(i).toString());
                            }
                        }
                    }
                }

                if(fragmentOtvoren.equals("autori")) {

                    String s=adapterView.getItemAtPosition(i).toString();
                    s = s.substring(0, s.indexOf("(") - 1);
                    id = db.dajIdAutora(s);
                    autori = db.autoriIzBaze();
                    knjiga = db.knjigeKategorije(id);
                    for(int t=0; t<knjiga.size(); t++) {
                        for(int ti=0; ti<knjiga.get(t).getAutori().size(); ti++) {
                            for (int j = 0; j < autori.size(); j++) {
                                if (knjiga.get(t).getAutori().get(ti).equals(autori.get(j))) {
                                    novaListaKnjiga.add(knjiga.get(t));
                                    id = db.dajIdAutora(autori.get(j).getIme());
                                }
                            }
                        }
                    }
                }

                bundle.putIntegerArrayList("listItemToSetBackground", itemToSetBg);
                bundle.putSerializable("knjiga1", knjiga);
                bundle.putSerializable("knjige", novaListaKnjiga);
                bundle.putStringArrayList("autoriNova", listaAutora);
                if (byteNiz != null) bundle.putByteArray("slika", byteNiz);
                bundle.putSerializable("knjiga1", knjiga);
                bundle.putSerializable("autori", autori);
                bundle.putStringArrayList("listakategorija", unosi);
                bundle.putString("fragment", fragmentOtvoren);

                bundle.putLong("id", id);

                ListeFragment frag = new ListeFragment();
                frag.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.mjestoF1, KnjigeFragment.newInstance(bundle)).commit();

            }
        });


        dugmeListaKategorija.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fragmentOtvoren = "kategorije";
                dugmeKategorija.setVisibility(View.VISIBLE);
                pretraga.setVisibility(View.VISIBLE);
                txt.setVisibility(View.VISIBLE);
                adapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.element_liste, R.id.tekst, unosi);
                lista.setAdapter(adapter);
                adapter.notifyDataSetChanged();

            }
        });

        dugmeListaAutora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fragmentOtvoren = "autori";
                if(listaAutora != null) listaAutora.clear();

              /*  if(autori != null) {
                    for (int i = 0; i < autori.size(); i++)
                        listaAutora.add(autori.get(i).getIme() + " (" + autori.get(i).getKolikoDjela() + ")");
                }
*/


                if(db.daLiJePraznaBaza2()) {
                    ArrayList<Autor> a = new ArrayList<>();
                    a=db.autoriIzBaze();

                    for (int i = 0; i < a.size(); i++)
                      listaAutora.add(a.get(i).getIme() + " (" + a.get(i).getKolikoDjela() + ")");
                    adapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.element_liste, R.id.tekst, listaAutora);
                    lista.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
                else {
                    adapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.element_liste, R.id.tekst, listaAutora);
                    lista.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }

                dugmeKategorija.setVisibility(View.GONE);
                pretraga.setVisibility(View.GONE);
                txt.setVisibility(View.GONE);
            }
        });


        dodajOnline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("kategorije", unosi);
                bundle.putSerializable("knjige", knjiga);
                bundle.putSerializable("autori", autori);
                bundle.putString("fragment", fragmentOtvoren);

                ListeFragment frag = new ListeFragment();
                frag.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();

                fragmentManager.beginTransaction().replace(R.id.mjestoF1, FragmentOnline.newInstance(bundle)).commit();

            }
        });

        return view;
    }

}

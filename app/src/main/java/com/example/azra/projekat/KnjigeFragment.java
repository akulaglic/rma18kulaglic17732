package com.example.azra.projekat;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Azra on 3.4.2018.
 */

public class KnjigeFragment extends Fragment {

    ArrayList<Knjiga> knjige = new ArrayList<>();
    ArrayList<Knjiga> knjiga1 = new ArrayList<>();
    ArrayList<String> kategorije = new ArrayList<>();
    private ArrayList<Integer> itemToSetBg = new ArrayList<>();
    private byte[] byteNiz;
    private ArrayList<String> listaAutora = new ArrayList<>();
    private String fragment = "kategorije";
    private ArrayList<Autor> autori = new ArrayList<>();
    private  long id;

    public static KnjigeFragment newInstance(Bundle bundle) {

        Bundle args = new Bundle();
        args = bundle;
        KnjigeFragment fragment = new KnjigeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.knjige_fragment, container, false);

        ImageView slika = (ImageView) view.findViewById(R.id.eNaslovna);
        Button dugmePovratak = (Button) view.findViewById(R.id.dPovratak);
        final ListView lista = (ListView) view.findViewById(R.id.listaKnjiga);

        final BazaOpenHelper db = new BazaOpenHelper(getActivity());

        if (getArguments() != null) {
            byteNiz = getArguments().getByteArray("slika");
            knjige = (ArrayList) getArguments().getSerializable("knjige");
            knjiga1 = (ArrayList) getArguments().getSerializable("knjiga1");
            kategorije = getArguments().getStringArrayList("listakategorija");
            if (getArguments().getIntegerArrayList("listItemToSetBackground") != null)
                itemToSetBg = getArguments().getIntegerArrayList("listItemToSetBackground");
            listaAutora = getArguments().getStringArrayList("autoriNova");
            fragment = getArguments().getString("fragment");
            autori = (ArrayList<Autor>) getArguments().getSerializable("autori");
            id = getArguments().getLong("id");
        }

        ArrayList<Knjiga> unosi = new ArrayList<>();

      if(!db.daLiJePraznaBaza1(id)) {
          unosi = db.knjigeKategorije(id);
          final KnjigaAdapter adapter = new KnjigaAdapter(getActivity(), R.layout.element_knjiga, unosi, itemToSetBg);
          lista.setAdapter(adapter);
          adapter.notifyDataSetChanged();
      }
       if (fragment.equals("kategorije")) {
            unosi = db.knjigeKategorije(id);
            final KnjigaAdapter adapter = new KnjigaAdapter(getActivity(), R.layout.element_knjiga, unosi, itemToSetBg);
            lista.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
        else if (fragment.equals("autori")) {
            unosi = db.knjigeAutora(id);
            final KnjigaAdapter adapter = new KnjigaAdapter(getActivity(), R.layout.element_knjiga, unosi, itemToSetBg);
            lista.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }

        ArrayList<Autor> a = new ArrayList<>();

       /* Knjiga k = new Knjiga();
        for (int i = 0; i < knjige.size(); i++) {
            k.setNaslovna(knjige.get(i).getNaslovna());
            k.setAutora(knjige.get(i).getAutora());
            k.setNaslovna(byteNiz);
            k.setSlika(knjige.get(i).getSlika());

            k.setDatumObjavljivanja(knjige.get(i).getDatumObjavljivanja());
            k.setOpis(knjige.get(i).getOpis());
            k.setBrojStranica(knjige.get(i).getBrojStranica());
        }
        for (int i = 0; i < knjige.size(); i++) {
            for (int j = 0; j < knjige.size(); j++) {
                if (knjige.get(j).getNazivKnjige().equals(knjige.get(i).getNazivKnjige()) && i != j) {
                    a.add(knjige.get(j).getAutora());
                }

            }
            k.setAutori(a);
            a.clear();
        }
*/


        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                itemToSetBg.add(i);
                View v = lista.getChildAt(i);
                TextView textView = (TextView) v.findViewById(R.id.nazivKnjige);
                String text = textView.getText().toString();
                TextView textView1 = (TextView) v.findViewById(R.id.eDatumObjavljivanja);
                String text1 = textView1.getText().toString();
                long idknj = db.dajIdKnjige(text, text1);


                db.promijeniPregledanoZaKnjigu(idknj);

                int myColor = ContextCompat.getColor(getContext(), R.color.colorPlavaSvijetla);
                if (lista.getChildAt(i) != null)
                    lista.getChildAt(i).setBackgroundColor(myColor);
            }
        });
        dugmePovratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();
                bundle.putSerializable("knjiga", knjiga1);
                bundle.putStringArrayList("listakategorija", kategorije);
                bundle.putSerializable("lista", knjige);
                bundle.putIntegerArrayList("listItemToSetBackground", itemToSetBg);
                if (byteNiz != null) bundle.putByteArray("slika", byteNiz);
                bundle.putStringArrayList("autoriNova", listaAutora);
                bundle.putString("fragment", fragment);
                bundle.putSerializable("autori", autori);

                KnjigeFragment frag = new KnjigeFragment();
                frag.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.mjestoF1, ListeFragment.newInstance(bundle)).commit();

            }
        });
        return view;
    }

    public boolean DaLiVecPostojiKnjiga(Knjiga k, ArrayList<Knjiga> unosi) {

        for (int i = 0; i < unosi.size(); i++)
            if (unosi.get(i).getNazivKnjige().equals(k.getNazivKnjige())
                    && unosi.get(i).getAutori().equals(k.getAutori())) return true;

        return false;
    }
}

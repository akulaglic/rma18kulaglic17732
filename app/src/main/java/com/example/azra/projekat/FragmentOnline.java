package com.example.azra.projekat;

import android.content.Intent;
import android.icu.text.LocaleDisplayNames;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.CharArrayReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Azra on 10.5.2018.
 */

public class FragmentOnline extends Fragment implements DohvatiKnjige.IDohvatiKnjigeDone, DohvatiNajnovije.IDohvatiNajnovijeDone, MojResultReceiver.Receiver {

    private ArrayList<String> kategorije = new ArrayList<>();
    private Spinner spinerRezultat;
    private ArrayList<Knjiga> listaKnjigaSaWeba = new ArrayList<>();
    private String fragmentOtvoren;
    private ArrayList<Knjiga> listaKnjiga = new ArrayList<>();
    private ArrayList<Autor> autori = new ArrayList<>();
    private MojResultReceiver mReceiver;


    public static FragmentOnline newInstance(Bundle bundle) {

        Bundle args = new Bundle();
        args = bundle;
        FragmentOnline fragment = new FragmentOnline();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_online, container, false);
        final BazaOpenHelper db = new BazaOpenHelper(getActivity());

        if (getArguments() != null && getArguments().containsKey("kategorije") && getArguments().containsKey("knjige")
                && getArguments().containsKey("autori")) {
            kategorije = getArguments().getStringArrayList("kategorije");
            listaKnjiga = (ArrayList<Knjiga>) getArguments().getSerializable("knjige");
            autori = (ArrayList<Autor>) getArguments().getSerializable("autori");
            fragmentOtvoren = getArguments().getString("fragment");
        }


        if (kategorije == null) kategorije = new ArrayList<>();
        if (listaKnjiga == null) listaKnjiga = new ArrayList<>();
        if (autori == null) autori = new ArrayList<>();

        final Button run = (Button) view.findViewById(R.id.dRun);
        final Button nazad = (Button) view.findViewById(R.id.dPovratak);
        final Button add = (Button) view.findViewById(R.id.dAdd);
        final EditText upit = (EditText) view.findViewById(R.id.tekstUpit);
        final Spinner spinerKategorije = view.findViewById(R.id.sKategorije);


        ArrayAdapter<String> adp1 = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_list_item_1, kategorije);
        adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinerKategorije.setAdapter(adp1);

        spinerRezultat = view.findViewById(R.id.sRezultat);

        run.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String tekst = upit.getText().toString();
                ArrayList<String> lista = new ArrayList<>();
                String rijec = "";
                for (int i = 0; i < tekst.length(); i++) {
                    if (tekst.charAt(i) != ';') rijec += tekst.charAt(i);
                    if (tekst.charAt(i) == ';' || i == tekst.length() - 1) {
                        lista.add(rijec);
                        rijec = "";
                    }
                    if (rijec.equals("autor:")) {

                        int duzina = tekst.length();
                        rijec = "";
                        while (i < duzina) {
                            rijec += tekst.charAt(i);
                            i++;
                        }

                        lista.add(rijec);
                        String[] imeAutora = new String[lista.size()];
                        for (int j = 0; j < lista.size(); j++) {
                            imeAutora[j] = lista.get(j);
                        }
                        ArrayList<String> djela = new ArrayList<>();
                        new DohvatiNajnovije((DohvatiNajnovije.IDohvatiNajnovijeDone) FragmentOnline.this).execute(imeAutora);
                        break;
                    }
                    if (i == tekst.length() - 1) {
                        String[] naziviKnjiga = new String[lista.size()];
                        for (int j = 0; j < lista.size(); j++) {
                            naziviKnjiga[j] = lista.get(j);
                        }
                        new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone) FragmentOnline.this).execute(naziviKnjiga);
                    }
                    if (rijec.equals("korisnik:")) {
                        Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), KnjigePoznanika.class);

                        mReceiver = new MojResultReceiver(new Handler());
                        mReceiver.setmReceiver(FragmentOnline.this);

                        int duzina = tekst.length();
                        i++;
                        rijec = "";
                        while (i < duzina) {
                            rijec += tekst.charAt(i);
                            i++;
                        }

                        intent.putExtra("korisnik", rijec);
                        intent.putExtra("receiver", mReceiver);
                        getActivity().startService(intent);

                    }
                }

            }
        });

        nazad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("knjiga", listaKnjiga);
                bundle.putStringArrayList("listakategorija", kategorije);
                bundle.putSerializable("autori", autori);
                bundle.putString("fragment", fragmentOtvoren);

                FragmentOnline frag = new FragmentOnline();
                frag.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.mjestoF1, ListeFragment.newInstance(bundle)).commit();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int djela=0;
                int i = spinerRezultat.getSelectedItemPosition();

                autori = db.autoriIzBaze();
                if(listaKnjigaSaWeba == null) Toast.makeText(getActivity(), "Greska, unesite knjigu!", Toast.LENGTH_LONG).show();
                else {
                    if (autori.isEmpty()) {
                        djela = 1;
                        ArrayList<String> naslovi = new ArrayList<>();
                        naslovi.clear();
                        naslovi.add(listaKnjigaSaWeba.get(i).getNazivKnjige());
                        for (int j = 0; j < listaKnjigaSaWeba.get(i).getAutori().size(); j++) {
                            Autor autorPravi = new Autor(listaKnjigaSaWeba.get(i).getAutori().get(j).getIme(), naslovi, djela);
                            autori.add(autorPravi);

                            Knjiga knjiga = new Knjiga(listaKnjigaSaWeba.get(i).getNazivKnjige(), autorPravi, null, spinerKategorije.getSelectedItem().toString());

                            knjiga.setSlika(listaKnjigaSaWeba.get(i).getSlika());

                            knjiga.setImenaAutora(listaKnjigaSaWeba.get(i).getAutora().getIme());

                            for(int k=0; k<listaKnjigaSaWeba.get(i).getAutori().size(); k++)
                            knjiga.dodajAutora(listaKnjigaSaWeba.get(i).getAutori().get(k));

                            knjiga.setId(listaKnjigaSaWeba.get(i).getId());
                            knjiga.setBrojStranica(listaKnjigaSaWeba.get(i).getBrojStranica());
                            knjiga.setOpis(listaKnjigaSaWeba.get(i).getOpis());
                            knjiga.setDatumObjavljivanja(listaKnjigaSaWeba.get(i).getDatumObjavljivanja());

                            listaKnjiga.add(knjiga);
                            db.dodajKnjigu(knjiga);

                        }
                    } else {
                        for (int j = 0; j < autori.size(); j++) {
                            for (int k = 0; k < listaKnjigaSaWeba.get(i).getAutori().size(); k++) {
                                if (autori.get(j).getIme().contains(listaKnjigaSaWeba.get(i).getAutori().get(k).getIme())) {
                                    autori.get(j).setKolikoDjela(autori.get(j).getKolikoDjela() + 1);
                                    autori.get(j).getDjelo().add(listaKnjigaSaWeba.get(i).getNazivKnjige());

                                    Knjiga knjiga = new Knjiga(listaKnjigaSaWeba.get(i).getNazivKnjige(), autori.get(j), null, spinerKategorije.getSelectedItem().toString());
                                    knjiga.setSlika(listaKnjigaSaWeba.get(i).getSlika());

                                    knjiga.setImenaAutora(listaKnjigaSaWeba.get(i).getAutora().getIme());

                                    for(int f=0; f<listaKnjigaSaWeba.get(i).getAutori().size(); f++) {
                                        //if(listaKnjigaSaWeba.get(i).getAutori().get(f).equals(knjiga.getAutori().get(t)))
                                        knjiga.dodajAutora(listaKnjigaSaWeba.get(i).getAutori().get(f));
                                    }

                                    knjiga.setId(listaKnjigaSaWeba.get(i).getId());
                                    knjiga.setBrojStranica(listaKnjigaSaWeba.get(i).getBrojStranica());
                                    knjiga.setOpis(listaKnjigaSaWeba.get(i).getOpis());
                                    knjiga.setDatumObjavljivanja(listaKnjigaSaWeba.get(i).getDatumObjavljivanja());

                                    knjiga = viseAutora(listaKnjiga, knjiga);
                                    db.dodajKnjigu(knjiga);
                                    listaKnjiga.add(knjiga);
                                }
                            }
                        }

                        for (int j = 0; j < listaKnjigaSaWeba.get(i).getAutori().size(); j++) {
                            if (!uListiJe(autori, listaKnjigaSaWeba.get(i).getAutori().get(j).getIme())) {
                                djela = 1;
                                ArrayList<String> naslovi = new ArrayList<>();
                                naslovi.clear();
                                naslovi.add(listaKnjigaSaWeba.get(i).getNazivKnjige());

                                Autor autorPravi = new Autor(listaKnjigaSaWeba.get(i).getAutori().get(j).getIme(), naslovi, djela);
                                autori.add(autorPravi);

                                Knjiga knjiga = new Knjiga(listaKnjigaSaWeba.get(i).getNazivKnjige(), autorPravi, null, spinerKategorije.getSelectedItem().toString());
                                knjiga.setSlika(listaKnjigaSaWeba.get(i).getSlika());

                                knjiga.setImenaAutora(listaKnjigaSaWeba.get(i).getAutora().getIme());

                                for (int k = 0; k < listaKnjigaSaWeba.get(i).getAutori().size(); k++)
                                    knjiga.dodajAutora(listaKnjigaSaWeba.get(i).getAutori().get(k));


                                knjiga.setId(listaKnjigaSaWeba.get(i).getId());
                                knjiga.setBrojStranica(listaKnjigaSaWeba.get(i).getBrojStranica());
                                knjiga.setOpis(listaKnjigaSaWeba.get(i).getOpis());
                                knjiga.setDatumObjavljivanja(listaKnjigaSaWeba.get(i).getDatumObjavljivanja());
                                knjiga = viseAutora(listaKnjiga, knjiga);
                                db.dodajKnjigu(knjiga);
                                listaKnjiga.add(knjiga);

                            }
                        }

                    }
                    upit.setText("");
                }

            }
        });

        return view;
    }


    private boolean uListiJe(ArrayList<Autor> autori, String ime) {
        for (Autor autor : autori) {
            if (autor.getIme().equals(ime)) return true;
        }
        return false;
    }

    @Override
    public void onDohvatiDone(ArrayList<Knjiga> rez) {

        listaKnjigaSaWeba = rez;
        ArrayList<String> lista = new ArrayList<>();

        for (Knjiga k : rez) {
            lista.add(k.getNazivKnjige());
        }

        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_list_item_1, lista);
        adp2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinerRezultat.setAdapter(adp2);
    }

    @Override
    public void onNajnovijeDone(ArrayList<Knjiga> rez) {
        listaKnjigaSaWeba = rez;
        ArrayList<String> lista = new ArrayList<>();

        for (Knjiga k : rez) {
            lista.add(k.getNazivKnjige());
        }

        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_list_item_1, lista);
        adp2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinerRezultat.setAdapter(adp2);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case KnjigePoznanika.STATUS_START:
                Toast.makeText(getActivity(), "Poziv poceo", Toast.LENGTH_SHORT).show();
                break;
            case KnjigePoznanika.STATUS_FINISHED:
                ArrayList<String> lista = new ArrayList<>();
                ArrayList<Knjiga> listaKnjiga = new ArrayList<>();
                listaKnjiga = (ArrayList) resultData.getParcelableArrayList("result");
                listaKnjigaSaWeba = listaKnjiga;
                for (Knjiga k : listaKnjiga) {
                    lista.add(k.getNazivKnjige());
                }
                ArrayAdapter<String> adp2 = new ArrayAdapter<String>(
                        getActivity().getApplicationContext(),
                        android.R.layout.simple_list_item_1, lista
                );
                adp2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinerRezultat.setAdapter(adp2);
                break;

            case KnjigePoznanika.STATUS_ERROR:
                Toast.makeText(getActivity(), "Greska", Toast.LENGTH_LONG).show();
        }
    }

    public Knjiga viseAutora (ArrayList<Knjiga> knjige, Knjiga k) {

        for (int i = 0; i < knjige.size(); i++) {

            for (int j = 0; j < knjige.size(); j++) {

                if (knjige.get(i).getNazivKnjige().equals(knjige.get(j).getNazivKnjige())
                        && knjige.get(i).getOpis().equals(knjige.get(j).getOpis())) {

                    knjige.get(i).dodajAutora(knjige.get(j).getAutora());

                }
                if(knjige.get(i).getNazivKnjige().equals(k.getNazivKnjige())
                        && knjige.get(i).getOpis().equals(k.getOpis()) && j==knjige.size()-1) {
                    knjige.get(i).dodajAutora(knjige.get(j).getAutora());
                    return knjige.get(i);
                }

            }


        }
        return k;
    }

}

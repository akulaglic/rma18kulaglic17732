package com.example.azra.projekat;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Azra on 2.4.2018.
 */

public class Autor implements Serializable {

    private String imeiPrezime;
    public ArrayList<String> knjige = new ArrayList<>();

    public int kolikoDjela;

    public Autor (String imeiPrezime, String id) {
        this.imeiPrezime = imeiPrezime;
        dodajKnjigu(id);
    }

    public Autor (String ime, ArrayList<String> djelo, int kolikoDjela) {
        this.imeiPrezime= ime;
        this.knjige = djelo;
        this.kolikoDjela = kolikoDjela;
    }

    public Autor () {
        this.kolikoDjela = 0;
        this.imeiPrezime = null;

    }

    public void dodajKnjigu(String id) {

        if(knjige!= null && !knjige.contains(id)) {
            knjige.add(id);
        }

    }

    public String getIme() {
        return imeiPrezime;
    }


    public int getKolikoDjela() {
        return kolikoDjela;
    }

    public void setIme(String ime) {
        this.imeiPrezime = ime;
    }

    public void setKolikoDjela(int kolikoDjela) {
        this.kolikoDjela = kolikoDjela;
    }

    public ArrayList<String> getDjelo() {
        return knjige;
    }

    public void setDjelo(ArrayList<String> djelo) {
        this.knjige = djelo;
    }

}


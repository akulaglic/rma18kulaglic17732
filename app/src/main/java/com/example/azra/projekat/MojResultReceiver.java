package com.example.azra.projekat;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

import java.security.PublicKey;

/**
 * Created by Azra on 13.5.2018.
 */

public class MojResultReceiver extends ResultReceiver {
    private Receiver mReceiver;

    public MojResultReceiver(Handler handler) {
        super(handler);
    }

    public interface Receiver {
        public void onReceiveResult(int resultCode, Bundle redultData);
    }

    public void setmReceiver(Receiver mReceiver) {
        this.mReceiver = mReceiver;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (mReceiver != null) {
            mReceiver.onReceiveResult(resultCode, resultData);
        }
    }

}
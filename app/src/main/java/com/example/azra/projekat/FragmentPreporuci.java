package com.example.azra.projekat;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaCas;
import android.media.tv.TvInputService;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.service.textservice.SpellCheckerService;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.util.ArrayList;
import java.util.Properties;

/**
 * Created by Azra on 23.5.2018.
 */

public class FragmentPreporuci extends Fragment {

    private ArrayList<String> lista =  new ArrayList<>();
    private ArrayList<String> spinerLista = new ArrayList<>();
    private String naslovKnjige;
    private Knjiga knjiga;
    private Spinner spiner;
    private String fragment = "kategorije";

    public static FragmentPreporuci newInstance(Bundle bundle) {

        Bundle args = new Bundle();
        args = bundle;
        FragmentPreporuci fragment = new FragmentPreporuci();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_preporuci, container, false);
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_CONTACTS}, 0);
        if (getArguments() != null) {
            naslovKnjige = getArguments().getString("naslov");
            knjiga = getArguments().getParcelable("knjiga");
            fragment = getArguments().getString("fragment");
        }

        final Button posalji = (Button) view.findViewById(R.id.dPosalji);
        final Button nazad = (Button) view.findViewById(R.id.dNazad);
        final TextView opis = (TextView) view.findViewById(R.id.opis1);
        final TextView opis1 = (TextView) view.findViewById(R.id.opis2);
        spiner = (Spinner) view.findViewById(R.id.sKontakti);
        spinerLista = getNameEmailDetails();
        posalji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendEmail();
            }
        });

        nazad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("fragment", fragment);
                bundle.putString("fragment", fragment);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.mjestoF1, KnjigeFragment.newInstance(bundle)).commit();
            }
        });

        if(knjiga.getAutori() != null) {
            String s = "Kategorija: "+  knjiga.getKategorija() + "\n" + "Naziv knjige: "+knjiga.getNazivKnjige() + "\n" +
                    "Autori: "+ knjiga.getImenaAutora() + "\n" + "Datum objavljivanja: "+ knjiga.getDatumObjavljivanja()
                    + "\n" + "Broj stranica: "+knjiga.getBrojStranica();
            opis.setText(s);
            String s2 = "Opis: "+knjiga.getOpis();
            opis1.setText(s2);
        }


        return view;
    }

    protected void sendEmail() {

        int index = spiner.getSelectedItemPosition();

        Log.i("Send email", "");
        String poruka;
        if(knjiga.getAutori() != null) poruka = "Zdravo " + spinerLista.get(index) + ", " + "\nProcitaj knjigu " +
                knjiga.getNazivKnjige() + " od " + knjiga.getAutori().get(0).getIme() + "!";

        else poruka = "Zdravo " + spinerLista.get(index) +", " + "\nProcitaj knjigu " +
                knjiga.getNazivKnjige() + " od " + knjiga.getAutora().getIme() + "!";
        Log.i("Send email", "");
        String TO = lista.get(index);

        String mailto = "mailto:" + TO + "?cc=" +  "&subject=" + Uri.encode("Preporuka za knjigu") +
                "&body=" + Uri.encode(poruka);

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse(mailto));

        try {
            startActivity(emailIntent);
        } catch (ActivityNotFoundException e) {
           Toast.makeText(getActivity(), "There is no email client installed.",
                   Toast.LENGTH_SHORT).show();
        }
    }

    public ArrayList<String> getNameEmailDetails(){
        if(getActivity().getApplicationContext().checkSelfPermission( Manifest.permission.READ_CONTACTS ) != PackageManager.PERMISSION_GRANTED )
            return new ArrayList<>();
        ArrayList<String> names = new ArrayList<String>();
        ContentResolver cr = getActivity().getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,null, null, null, null);
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                Cursor cur1 = cr.query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                        new String[]{id}, null);
                while (cur1.moveToNext()) {
                    //to get the contact names
                    String name=cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    Log.e("Name :", name);
                    String email = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));

                    Log.e("Email", email);
                    if(email!=null){
                        names.add(name);
                        lista.add(email);
                    }
                }
                cur1.close();
            }
        }

        ArrayAdapter<String> adp1 = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_list_item_1, lista);
        adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spiner.setAdapter(adp1);
        return names;
    }

    public interface CommunicationInterface {
        Knjiga getKnjiga();
    }

}


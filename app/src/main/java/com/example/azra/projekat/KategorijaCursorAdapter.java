package com.example.azra.projekat;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.widget.ResourceCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Azra on 24.5.2018.
 */

public class KategorijaCursorAdapter extends ResourceCursorAdapter {

    private final int layout;

    public KategorijaCursorAdapter(Context context, int layout, Cursor c, int flags) {
        super(context, layout, c, flags);
        this.layout = layout;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.element_liste, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        //view = newView(context, cursor, null);
        TextView kategorija = (TextView) view.findViewById(R.id.tekst);
        String s = cursor.getString(cursor.getColumnIndex(BazaOpenHelper.KATEGORIJE_NAZIV));
        kategorija.setText("ne");



    }
}

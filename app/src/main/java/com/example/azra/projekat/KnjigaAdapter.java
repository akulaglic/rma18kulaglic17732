package com.example.azra.projekat;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.SupportActivity;
import android.support.v4.content.ContextCompat;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Azra on 21.3.2018.
 */

public class KnjigaAdapter extends ArrayAdapter<Knjiga> implements FragmentPreporuci.CommunicationInterface{
    private final Context context;
    private final ArrayList<Knjiga> data;
    private final int layoutResourceId;
    private ArrayList<Integer> newPosition;
    private static int br = 0;
    private  Knjiga person;
    private String fragment = "kategorije";

    public KnjigaAdapter(Context context, int layoutResourceId, ArrayList<Knjiga> data, ArrayList<Integer> newPosition) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.data = data;
        this.layoutResourceId = layoutResourceId;
        this.newPosition = newPosition;
    }

    public KnjigaAdapter(Context context, int layoutResourceId, ArrayList<Knjiga> data) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.data = data;
        this.layoutResourceId = layoutResourceId;
        this.newPosition = null;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;

        BazaOpenHelper db = new BazaOpenHelper(context);

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            if (newPosition != null)

                for (int i = 0; i < newPosition.size(); i++) {
                    if (position == newPosition.get(i)) {
                        int myColor = ContextCompat.getColor(getContext(), R.color.colorPlavaSvijetla);
                        row.setBackgroundColor(myColor);
                    }
                }

            for (int i = 0; i < newPosition.size(); i++) {
                if (position == newPosition.get(i)) {
                    int myColor = ContextCompat.getColor(getContext(), R.color.colorPlavaSvijetla);
                    row.setBackgroundColor(myColor);
                }
            }

            holder = new ViewHolder();

            holder.textView1 = (TextView) row.findViewById(R.id.imeAutora);
            holder.textView2 = (TextView) row.findViewById(R.id.nazivKnjige);
            holder.textView3 = (TextView) row.findViewById(R.id.eDatumObjavljivanja);
            holder.textView4 = (TextView) row.findViewById(R.id.eOpis);
            //holder.textView4.setMovementMethod(new ScrollingMovementMethod());

            holder.textView5 = (TextView) row.findViewById(R.id.eBrojStranica);
            holder.slika = row.findViewById(R.id.eNaslovna);
            holder.dugme = (Button) row.findViewById(R.id.dPreporuci);

            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

         person = data.get(position);
        final int p = position;
        holder.textView2.setText(person.getNazivKnjige());
        holder.textView3.setText(person.getDatumObjavljivanja());
        holder.textView4.setText(person.getOpis());
        //holder.textView4.setMovementMethod(new ScrollingMovementMethod());

        holder.textView5.setText(String.valueOf(person.getBrojStranica()));

        //if(person.getAutori() != null) holder.textView1.setText(person.getAutoriZaListu().get(person.getAutoriZaListu().size() - 1).getIme());
         holder.textView1.setText(person.getImenaAutora());
        if (person.getSlika() != null)
            Picasso.get().load(String.valueOf(person.getSlika())).into(holder.slika);
        if (person.getNaslovna() != null)
            holder.slika.setImageBitmap(BitmapFactory.decodeByteArray(person.getNaslovna(), 0, person.getNaslovna().length));


        holder.dugme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString( "naslov", person.getNazivKnjige());
                bundle.putParcelable( "knjiga", data.get(p));
                bundle.putString("fragment", fragment);
                FragmentManager fragmentManager = ((KategorijeAkt) context).getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.mjestoF1, FragmentPreporuci.newInstance(bundle)).commit();

            }
        });


        return row;

    }

    @Override
    public Knjiga getKnjiga() {
        return person;
    }

    static class ViewHolder {
        TextView textView1;
        TextView textView2;
        TextView textView3;
        TextView textView4;
        TextView textView5;
        ImageView slika;
        Button dugme;
    }
}
package com.example.azra.projekat;

import android.graphics.Bitmap;
import android.media.Image;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;

/**
 * Created by Azra on 21.3.2018.
 */

public class Knjiga implements Parcelable {
    private String id;
    private String naziv;
    private ArrayList<Autor> autori = new ArrayList<>();
    private String opis;
    private String datumObjavljivanja;
    private URL slika;
    private int brojStranica;

    private Autor autor;
    private byte[] naslovna;
    private String kategorija;
    private String imenaAutora;

    public Knjiga(String id, String naziv, ArrayList<Autor> autori, String opis, String datumObjavljivanja, URL slika, int brojStranica) {
        this.id = id;
        this.naziv = naziv;
        this.autori = autori;
        this.opis = opis;
        this.datumObjavljivanja = datumObjavljivanja;
        this.slika = slika;
        this.brojStranica = brojStranica;
    }

    public Knjiga(String nazivKnjige) {
        this.naziv = nazivKnjige;
        this.autor = null;
        this.naslovna = null;
        this.kategorija = null;
    }

    public Knjiga(String nazivKnjige, Autor autor, byte[] naslovna, String kategorija) {
        this.naziv = nazivKnjige;
        this.autor = autor;
        this.naslovna = naslovna;
        this.kategorija = kategorija;
    }

    public Knjiga(String nazivKnjige, ArrayList<Autor> autori, byte[] naslovna, String kategorija) {
        this.naziv = nazivKnjige;
        this.autori = autori;
        this.naslovna = naslovna;
        this.kategorija = kategorija;
    }

    public Knjiga() {
        this.naziv = null;
        this.autor = null;
        this.naslovna = null;
        this.kategorija = null;
    }

    protected Knjiga(Parcel in) {
        id = in.readString();
        naziv = in.readString();
        opis = in.readString();
        datumObjavljivanja = in.readString();
        brojStranica = in.readInt();
        naslovna = in.createByteArray();
        kategorija = in.readString();
    }

    public String getImenaAutora() {
        return imenaAutora;
    }

    public void setImenaAutora(String imenaAutora) {
        this.imenaAutora = imenaAutora;
    }

    public static final Creator<Knjiga> CREATOR = new Creator<Knjiga>() {
        @Override
        public Knjiga createFromParcel(Parcel in) {
            return new Knjiga(in);
        }

        @Override
        public Knjiga[] newArray(int size) {
            return new Knjiga[size];
        }
    };

    public String getDatumObjavljivanja() {
        return datumObjavljivanja;
    }

    public void setDatumObjavljivanja(String datumObjavljivanja) {
        this.datumObjavljivanja = datumObjavljivanja;
    }

    public URL getSlika() {
        return slika;
    }

    public void setSlika(URL slika) {
        this.slika = slika;
    }

    public String getOpis() {
        return opis;
    }

    public int getBrojStranica() {
        return brojStranica;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public void setBrojStranica(int brojStranica) {
        this.brojStranica = brojStranica;
    }

    public ArrayList<Autor> getAutori() {
        return autori;
    }

    public void setAutori(ArrayList<Autor> autori) {
        this.autori = autori;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Autor getAutora() {
        return autor;
    }

    public byte[] getNaslovna() {
        return naslovna;
    }

    public String getNazivKnjige() {
        return naziv;
    }

    public String getKategorija() {
        return kategorija;
    }

    public void setAutora(Autor autor) {
        this.autor = autor;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }

    public void setNazivKnjige(String nazivKnjige) {
        this.naziv = nazivKnjige;
    }

    public void setNaslovna(byte[] naslovna) {
        this.naslovna = naslovna;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(naziv);
        parcel.writeString(opis);
        parcel.writeString(datumObjavljivanja);
        parcel.writeInt(brojStranica);
        parcel.writeByteArray(naslovna);
        parcel.writeString(kategorija);
    }



    public void dodajAutora(Autor a) {
        autori.add(a);
    }
}

package com.example.azra.projekat;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Azra on 1.4.2018.
 */

public class DodavanjeKnjigeFragment extends Fragment {

    private static int PICK_IMAGE_REQUEST = 1;
    private ImageView slika;
    private ArrayList<Knjiga> listaKnjiga = new ArrayList<>();
    private ArrayList<Autor> listaAutora = new ArrayList<>();
    private Bitmap bitmapSlika;
    private byte[] byteNiz;
    private ArrayList<String> kategorije = new ArrayList<>();
    private ArrayList<Autor> autori = new ArrayList<>();
    private Knjiga k = new Knjiga();

    public static DodavanjeKnjigeFragment newInstance(Bundle bundle) {

        Bundle args = new Bundle();
        DodavanjeKnjigeFragment fragment = new DodavanjeKnjigeFragment();
        args = bundle;
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dodavanje_knjige_fragment, container, false);


        if (getArguments() != null && getArguments().containsKey("kategorije") && getArguments().containsKey("knjige")
                && getArguments().containsKey("autori")) {
            kategorije = getArguments().getStringArrayList("kategorije");
            listaKnjiga = (ArrayList<Knjiga>) getArguments().getSerializable("knjige");
            autori = (ArrayList<Autor>) getArguments().getSerializable("autori");

        }

        if (kategorije == null) kategorije = new ArrayList<>();
        if (listaKnjiga == null) listaKnjiga = new ArrayList<>();
        if (autori == null) autori = new ArrayList<>();

        Button dugmeUpisi = (Button) view.findViewById(R.id.dUpisiKnjigu);
        Button dugmeNadji = (Button) view.findViewById(R.id.dNadjiSliku);
        final Spinner spiner = (Spinner) view.findViewById(R.id.sKategorijaKnjige);
        Button dugmePonisti = (Button) view.findViewById(R.id.dPonisti);
        final EditText autor = (EditText) view.findViewById(R.id.imeAutora);
        final EditText naslov = (EditText) view.findViewById(R.id.nazivKnjige);
        slika = (ImageView) view.findViewById(R.id.naslovnaStr);

        ArrayAdapter<String> adp1 = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_list_item_1, kategorije);
        adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spiner.setAdapter(adp1);
        dugmePonisti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();
                bundle.putSerializable("knjiga", listaKnjiga);
                bundle.putStringArrayList("listakategorija", kategorije);
                if (byteNiz != null) bundle.putByteArray("slika", byteNiz);
                bundle.putSerializable("autori", autori);

                DodavanjeKnjigeFragment frag = new DodavanjeKnjigeFragment();
                frag.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.mjestoF1, ListeFragment.newInstance(bundle)).commit();

            }
        });

        dugmeUpisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (kategorije.isEmpty()) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                    alertDialog.setTitle("Upozorenje!");
                    alertDialog.setMessage("Lista kategorija je prazna!");
                    alertDialog.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(getActivity().getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
                                }
                            });
                    alertDialog.show();
                } else if (naslov == null || autor == null || byteNiz == null) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                    alertDialog.setTitle("Upozorenje!");
                    alertDialog.setMessage("Popunite sva polja!");
                    alertDialog.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(getActivity().getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
                                }
                            });
                    alertDialog.show();
                } else {

                    int djela = 0;

                    if (autori.isEmpty()) {
                        djela = 1;
                        ArrayList<String> naslovi = new ArrayList<>();
                        if (naslovi != null) naslovi.clear();
                        naslovi.add(naslov.getText().toString());
                        Autor autorPravi = new Autor(autor.getText().toString(), naslovi, djela);
                        autori.add(autorPravi);

                        Knjiga knjiga = new Knjiga(naslov.getText().toString(), autorPravi, byteNiz, spiner.getSelectedItem().toString());
                        listaKnjiga.add(knjiga);
                    } else {

                        for (int i = 0; i < autori.size(); i++) {

                            if (autori.get(i).getIme().equals(autor.getText().toString())) {

                                autori.get(i).setKolikoDjela(autori.get(i).getKolikoDjela() + 1);
                                autori.get(i).getDjelo().add(naslov.getText().toString());

                                Knjiga knjiga = new Knjiga(naslov.getText().toString(), autori.get(i), byteNiz, spiner.getSelectedItem().toString());
                                listaKnjiga.add(knjiga);
                                continue;

                            }
                        }

                        if (!uListiJe(autori, autor.getText().toString())) {
                            djela = 1;
                            ArrayList<String> naslovi = new ArrayList<>();
                            if (naslovi != null) naslovi.clear();
                            naslovi.add(naslov.getText().toString());
                            Autor autorPravi = new Autor(autor.getText().toString(), naslovi, djela);
                            autori.add(autorPravi);

                            Knjiga knjiga = new Knjiga(naslov.getText().toString(), autorPravi, byteNiz, spiner.getSelectedItem().toString());
                            listaKnjiga.add(knjiga);

                        }
                    }
                    autor.setText("");
                    naslov.setText("");
                }
            }
        });

        dugmeNadji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivityForResult(intent, PICK_IMAGE_REQUEST);
                }
            }

        });

        return view;
    }


    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor = getActivity().getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            bitmapSlika = getBitmapFromUri(data.getData());
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmapSlika.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byteNiz = stream.toByteArray();
            slika.setImageBitmap(bitmapSlika);
        } catch (IOException e) {
            e.getMessage();
        }

    }

    private boolean uListiJe(ArrayList<Autor> autori, String ime) {
        for (Autor autor : autori) {
            if (autor.getIme().equals(ime)) return true;
        }
        return false;
    }

}
